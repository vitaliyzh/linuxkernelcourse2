Task #7. Time Management∗

Description∗
    Using time management in a kernel module.

Guidance∗

    Create a kernel module to take the time in seconds since the last read using procfs/sysfs interface.

    Implement the ability to change the time format form seconds to hh:mm:ss.

    Add the ability to get the absolute time in the format hh:mm:ss.

    Additionally set the current time.

Extra∗

    Add the ability to get a random value in a given range every N seconds.
