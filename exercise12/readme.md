Exercise #12. Board Configuration. Device Tree. ACPI∗

Description∗
    Working with the DS3231.

References∗
    DS1307 real-time clock.
    DS3231 High Accuracy RTC for Raspberry Pi.
    
Guidance∗

    Connect a board to SBC.
    Detect i2c device.
    Read data from the device.
