Exercise #8. Linux Kernel: Overview, Structure, Building, Installing

Extra



1. Perform cross-compilation of a test system.

vitaliy@vitaliy-HP-ZBook-15-G5:~/projects/exercise08$ make -f ./Makefile_2 
echo kernel/linux-stable/my01
kernel/linux-stable/my01
make -C kernel/linux-stable/my01 M=/home/vitaliy/projects/exercise08 modules
make[1]: Entering directory '/home/vitaliy/DataSsd/projects/exercise08/kernel/linux-stable/my01'
  CC [M]  /home/vitaliy/projects/exercise08/ex01.o
  MODPOST /home/vitaliy/projects/exercise08/Module.symvers
  CC [M]  /home/vitaliy/projects/exercise08/ex01.mod.o
  LD [M]  /home/vitaliy/projects/exercise08/ex01.ko
make[1]: Leaving directory '/home/vitaliy/DataSsd/projects/exercise08/kernel/linux-stable/my01'
vitaliy@vitaliy-HP-ZBook-15-G5:~/projects/exercise08$ modinfo ./ex01.ko
filename:       /home/vitaliy/projects/exercise08/./ex01.ko
version:        0.01
description:    A simple example Linux module.
author:         xone
license:        GPL
srcversion:     CCD26AAFBB363EDE9DD03AD
depends:        
retpoline:      Y
name:           ex01
vermagic:       5.14.0-rc6 SMP mod_unload 686 
vitaliy@vitaliy-HP-ZBook-15-G5:~/projects/exercise08$ scp -P 8022 ./ex01.ko user@localhost:
user@localhost's password: 
ex01.ko    



2. Launch the test system in QEMU and check the ssh connection from the host system.



3. Display complete information about the built system (kernel version, user and host name, compiler version, build date and time).

italiy@vitaliy-HP-ZBook-15-G5:~/projects/exercise08/kernel$ ssh -p 8022 user@localhost
user@localhost's password: 
$ uname -r
5.14.0-rc6
$ whoami 
user
$ host
hostid    hostname  
$ hostname
myLinux
$ gcc --version
-bash: gcc: command not found
$ gcc -v       
-bash: gcc: command not found
$ ls -la 
total 6
drwxr-xr-x    2 user     user          1024 Aug 20 07:43 .
drwxr-xr-x    3 root     root          1024 Aug 20 06:55 ..
-rw-------    1 user     user            13 Aug 20 07:42 .bash_history
-rw-------    1 user     user          2840 Aug 20 07:43 ex01.ko
$ 

main system
vitaliy@vitaliy-HP-ZBook-15-G5:~/projects/exercise08$ gcc -v
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/8/lto-wrapper
OFFLOAD_TARGET_NAMES=nvptx-none
OFFLOAD_TARGET_DEFAULT=1
Target: x86_64-linux-gnu
Configured with: ../src/configure -v --with-pkgversion='Ubuntu 8.3.0-6ubuntu1' --with-bugurl=file:///usr/share/doc/gcc-8/README.Bugs --enable-languages=c,ada,c++,go,brig,d,fortran,objc,obj-c++ --prefix=/usr --with-gcc-major-version-only --program-suffix=-8 --program-prefix=x86_64-linux-gnu- --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --libdir=/usr/lib --enable-nls --enable-bootstrap --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --with-default-libstdcxx-abi=new --enable-gnu-unique-object --disable-vtable-verify --enable-libmpx --enable-plugin --enable-default-pie --with-system-zlib --with-target-system-zlib --enable-objc-gc=auto --enable-multiarch --disable-werror --with-arch-32=i686 --with-abi=m64 --with-multilib-list=m32,m64,mx32 --enable-multilib --with-tune=generic --enable-offload-targets=nvptx-none --without-cuda-driver --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu
Thread model: posix
gcc version 8.3.0 (Ubuntu 8.3.0-6ubuntu1) 
vitaliy@vitaliy-HP-ZBook-15-G5:~/projects/exercise08$ lsb_release -a
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 19.04
Release:        19.04
Codename:       disco

4. Document previous steps as a short instruction "for dummies" to building and installing the test system for your Linux distribution.


prepare base system:
git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable
cd linux-stable
export BUILD_KERNEL=my01
mkdir my01
make ARCH=i386 O=${BUILD_KERNEL} defconfig
cd ${BUILD_KERNEL}
make menuconfig
sudo apt install libelf-dev
make -j6

prepare root system:
git clone git://git.buildroot.net/buildroot
cd buildroot
export BUILD_ROOTFS=my01
make O=${BUILD_ROOTFS} qemu_x86_defconfig
cd ${BUILD_ROOTFS}
make menuconfig
make -j6

run:
qemu-system-i386 -kernel linux-stable/my01/arch/i386/boot/bzImage -append "root=/dev/sda console=ttyS0" -drive format=raw,file=buildroot/my01/images/rootfs.ext3 -nic user,hostfwd=tcp::8022-:22 &

connect:
ssh -p 8022 user@localhost

copy files:
scp -P 8022 ./ex01.ko user@localhost:

5. Save the instruction in the exercise08/readme.md file of your git repository.

fin
