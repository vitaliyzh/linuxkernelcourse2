#!/bin/bash
set -x

#define vars
VALUE=1
MAXVALUE=1
ATTEMPS=1

#check correct max value
check_max_value()
{
    if [[ $MAXVALUE -ge 1 && $MAXVALUE -le 100 ]]; then
        echo maxvalue: $MAXVALUE
    else
        echo "Enter correct max value"
        exit 0
    fi
}

#check correct value
check_value()
{
    if [ -n "$VALUE" ] && [ "$VALUE" -eq "$VALUE" ] 2>/dev/null; then
        echo value: $MAXVALUE
    else
        echo "Enter correct value"
        exit 0
    fi
}

#check correct atteps value
check_attemps()
{
    if [ -n "$ATTEMPS" ] && [ "$ATTEMPS" -eq "$ATTEMPS" ] 2>/dev/null; then
        echo attemps: $ATTEMPS
    else
        echo "Enter correct number attemps"
        exit 0
    fi
}

# read attempts value
read_attempts_value()
{
    read -p "Enter Attemps : " ATTEMPTS
    if [ -n "$ATTEMPTS" ] && [ "$ATTEMPTS" -eq "$ATTEMPTS" ] 2>/dev/null; then
	echo
    else
	echo "Enter correct number"
	exit 0;
    fi
}

# read max value
read_max_value()
{
    read -p "Enter Maximum limit 1..100 : " MAXVALUE
    if [ -n "$MAXVALUE" ] && [ "$MAXVALUE" -eq "$MAXVALUE" ] 2>/dev/null; then
	check_max_value
    else
	echo "Enter correct number"
	exit 0;
    fi
}

play_game()
{
    #lets play this game
    ./test.sh $VALUE $MAXVALUE $ATTEMPS
}


until [ -z "$1" ]
do
  #echo -n "$1 "
  arg=$1
  echo "$arg "
  shift
  case $arg in
    "-r")
        ATTEMPS=$1
        echo ATTEMPS $ATTEMPS
        check_attemps
        shift
        continue
        ;;
    "-u")
        MAXVALUE=$1
        echo MAXVALUE $MAXVALUE
        check_max_value
        shift
        continue
        ;;
    esac
done

echo

PS3='Please enter your choice: '
options=("Play" "Options" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Play")
            echo "you chose play"
            play_game
            ;;
        "Options")
            echo "you chose options"
            PS3='Please enter your choice: '
            options2=("Number of attempts" "Max value")
            select opt2 in "${options2[@]}"
            do
                case $opt2 in
                    "Number of attempts")
                        echo "you chose number of attemps"
                        read_attempts_value
                        break
                        ;;
                    "Max value")
                        echo "you chose max value"
                        read_max_value
                        break
                        ;;
                esac
            done
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
