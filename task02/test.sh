#!/bin/bash
#set -x

#define vars
VALUE=0
MAXVALUE=0
ATTEMPS=0

#show params count
echo parameters: $#

#check correct max value
check_max_value()
{
    if [[ $MAXVALUE -ge 1 && $MAXVALUE -le 100 ]]; then
        echo maxvalue: $MAXVALUE
    else
        echo "Enter correct max value"
        exit 0
    fi
}

#check correct value
check_value()
{
    if [ -n "$VALUE" ] && [ "$VALUE" -eq "$VALUE" ] 2>/dev/null; then
        echo value: $MAXVALUE
    else
        echo "Enter correct value"
        exit 0
    fi
}

#check correct atteps value
check_attemps()
{
    if [ -n "$ATTEMPS" ] && [ "$ATTEMPS" -eq "$ATTEMPS" ] 2>/dev/null; then
        echo attemps: $ATTEMPS
    else
        echo "Enter correct number attemps"
        exit 0
    fi
}

# read value
read_value()
{
    read -p "Enter Number: " VALUE
    if [ -n "$VALUE" ] && [ "$VALUE" -eq "$VALUE" ] 2>/dev/null; then
	echo
    else
	echo "Enter correct number"
	exit 0;
    fi
}

# read max value
read_max_value()
{
    read -p "Enter Maximum limit 1..100 : " MAXVALUE
    if [ -n "$MAXVALUE" ] && [ "$MAXVALUE" -eq "$MAXVALUE" ] 2>/dev/null; then
	check_max_value
    else
	echo "Enter correct number"
	exit 0;
    fi
}

# read attempts value
read_attempts_value()
{
    read -p "Enter Attemps : " ATTEMPTS
    if [ -n "$ATTEMPTS" ] && [ "$ATTEMPTS" -eq "$ATTEMPTS" ] 2>/dev/null; then
	echo
    else
	echo "Enter correct number"
	exit 0;
    fi
}


if [[ "$#" -eq 0 ]]; then
#check params count and try recv 3 params from user
    read_value
    read_max_value
    read_attempts_value
elif [[ "$#" -eq 1 ]]; then
#check params count and try recv 2 params from user
    read_max_value
    read_attempts_value
else
#check params
    VALUE=$1
    MAXVALUE=$2
    ATTEMPS=$3
    check_value
    check_max_value
    check_attemps
fi

TRY_AGAIN=1
while [[ $TRY_AGAIN -eq 1 ]]; do
    #do while COUNTER less ATTEMPS
    COUNTER=1
    while [[ $COUNTER -le $ATTEMPS ]]; do
	echo try $COUNTER
	# call my ./my_random.sh with parameter MAXVALUE
	./my_random.sh $MAXVALUE
	retn_code=$? #analize ret code
	echo $VALUE compare with $retn_code

	#compare values
	if [[ $VALUE -eq $retn_code ]]; then
	    echo "$VALUE is equal to $ret_code"
	    read -p "Try again? Enter y or n : " ANSWER
	    if [ "$ANSWER" = "y" ]; then
		TRY_AGAIN=1
	    else
		TRY_AGAIN=0
	    fi
	elif [[ $VALUE -le $retn_code ]]; then
	    echo "$VALUE is less than $ret_code"
	    TRY_AGAIN=0
	elif [[ $VALUE -gt $retn_code ]]; then
	    echo "$VALUE is greater than $ret_code"
	    TRY_AGAIN=0
	else
	    echo "undefine"
	    TRY_AGAIN=0
	fi
	#inc counter
	let COUNTER+=1
    done
done
#thats all :)
