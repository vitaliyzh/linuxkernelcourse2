clang -fsanitize=leak -fsanitize=address -g ./src/pak.c ; ASAN_OPTIONS=detect_leaks=1 ./src/bin/pak

echo "//////////////////////////////////////////////////////////////////////////"

clang -fsanitize=leak -fsanitize=address -g ./src/upak.c ; ASAN_OPTIONS=detect_leaks=1 ./src/bin/upak
