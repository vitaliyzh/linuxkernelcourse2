#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 1024

void SW_Version() { puts("Packer: version 1.0"); }

void Usage() {
  SW_Version();
  puts("Usage: options>");
  puts("Options are:");
  puts("  --debug ..          debug");
  puts("  --version           version");
  puts("  --help              command line help");
}

int main(int argc, char *argv[]) {
  int i;
  char stdin_buf[16384];
  bool isVerboseMode = false;

  if (argc > 1) {
    for (i = 1; i < argc; ++i) {
      if (!strcmp(argv[i], "debug"))
        isVerboseMode = true;
      else if (!strcmp(argv[i], "version"))
        SW_Version();
      else
        Usage();
    }
  }
  setvbuf(stdin, stdin_buf, _IOFBF, 16384);

  char buf[MAX_LEN];
  char resbuf[MAX_LEN * 2];
  memset(buf, 0, MAX_LEN);
  memset(resbuf, 0, MAX_LEN);

  if (!fgets(buf, MAX_LEN, stdin))
    goto EXIT_A;

  if (isVerboseMode) {
    for (i = 0; i < MAX_LEN; ++i) {
      if (buf[i] != 0)
        printf("%c", buf[i]);
    }
    printf("\n");
  }
  int count = 1;
  int pos = 0;
  for (i = 0; i < MAX_LEN - 1; ++i) {
    if (buf[i] == 0)
      break;

    if (buf[i] == buf[i + 1] && count < 9) {
      count++;
    } else if (buf[i] == buf[i + 1] && count == 9) {
      sprintf(&resbuf[pos], "%c%d", buf[i], count);
      pos += 2;
      count = 1;
    } else {
      sprintf(&resbuf[pos], "%c%d", buf[i], count);
      pos += 2;
      count = 1;
    }
  }
  if (count == 1) {
    resbuf[pos++] = buf[MAX_LEN - 1];
  } else {
    sprintf(&resbuf[pos], "%c%d", buf[MAX_LEN - 1], count);
    pos += 2;
  }

  if (isVerboseMode) {
    printf("res: %d\n", pos);
  }
  for (i = 0; i < pos; ++i) {
    if (resbuf[i] != 0)
      printf("%c", resbuf[i]);
  }
  if (isVerboseMode) {
    printf("\n");
  }
EXIT_A:
  return 0;
}
