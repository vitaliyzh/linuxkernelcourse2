#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 1024

void SW_Version() { puts("UnPacker: version 1.0"); }

void Usage() {
  SW_Version();
  puts("Usage: options>");
  puts("Options are:");
  puts("  --debug ..          debug");
  puts("  --version           version");
  puts("  --help              command line help");
}

int main(int argc, char *argv[]) {
  int i, j;
  char stdin_buf[16384];
  int c;
  bool isVerboseMode = false;

  if (argc > 1) {
    for (i = 1; i < argc; ++i) {
      if (!strcmp(argv[i], "debug"))
        isVerboseMode = true;
      else if (!strcmp(argv[i], "version"))
        SW_Version();
      else
        Usage();
    }
  }
  setvbuf(stdin, stdin_buf, _IOFBF, 16384);

  char buf[MAX_LEN];
  char resbuf[MAX_LEN * 2];
  memset(buf, 0, MAX_LEN);
  memset(resbuf, 0, MAX_LEN);

  if (!fgets(buf, MAX_LEN, stdin))
    goto EXIT_A;

  if (isVerboseMode) {
    for (i = 0; i < MAX_LEN; ++i) {
      printf("%c", buf[i]);
    }
    printf("\n");
  }

  int count = 0;
  int pos = 0;
  for (i = 0; i < MAX_LEN - 1; ++i) {
    if (buf[i] == 0)
      break;
    char c;
    sscanf(&buf[i], "%c%d", &c, &count);

    if (isVerboseMode)
      printf("%c %c%d\n", buf[i], c, count);

    for (j = 0; j < count; ++j) {
      resbuf[pos++] = c;
    }
    i++;
  }

  if (isVerboseMode) {
    printf("res: %d\n", pos);
  }
  for (i = 0; i < pos; ++i) {
    printf("%c", resbuf[i]);
  }
  if (isVerboseMode) {
    printf("\n");
  }
EXIT_A:
  return 0;
}
