#!/bin/bash 
#-x

GPIO_PATH=/sys/class/gpio

trap control_c SIGINT

control_c()
{
    echo "disable gpio"
    set_port 26 0
    echo 26 > $GPIO_PATH/unexport
    exit 0
}


init_port()
{
    PORT=$1
    DIRECT=$2
    echo "init port $PORT $DIRECT"
    if [ ! -d $GPIO_PATH/gpio$PORT ]; then
    {
       echo "enable gpio"
       `echo $PORT > $GPIO_PATH/export`
       sleep 0.1
       `echo $DIRECT > $GPIO_PATH/gpio$PORT/direction`
    }
    fi
}

set_port()
{
   ARG1=$1
   ARG2=$2
   #echo $ARG1 $ARG2
   echo $ARG2 > $GPIO_PATH/gpio$ARG1/value
}

HEX_TO_DEC()
{
    echo "ibase=16; $@"|bc
}

get_time()
{
  SEC=`i2cget -y 1 0x68 0x00`
  MIN=`i2cget -y 1 0x68 0x01`
  HOUR=`i2cget -y 1 0x68 0x02`

  DAY=`i2cget -y 1 0x68 0xA`
  MON=`i2cget -y 1 0x68 0x5`
  YEAR=`i2cget -y 1 0x68 0x06`
 
  TEMP=`i2cget -y 1 0x68 0x11`
  TEMP2=`i2cget -y 1 0x68 0x12`

  echo Time: $YEAR/$MON/$DAY   $HOUR:$MIN:$SEC
  printf "Time: %d/%d/%d %d:%d:%d\n" $YEAR $MON $DAY $HOUR $MIN $SEC
  let TEMP3=$TEMP / 256
  let TEMP4=$TEMP2 / 256
  echo Temp: $TEMP $TEMP2 $TEMP3 $TEMP4
  printf "Temp: %d \n" $TEMP
}

init_port 26 in

COUNTER=0
OLD_VAL=0
VAL=0
GET=0
BUTTOM_PRESSED=0
while [ 1 ]; do
  VAL=$(cat $GPIO_PATH/gpio26/value)
  if [ $OLD_VAL -eq 0 ] && [ $VAL -eq 1 ]; then
    GET=1
    OLD_VAL=$VAL
  else
    OLD_VAL=0
  fi

  if [ $VAL -eq 1 ]; then
    let BUTTOM_PRESSED=$BUTTOM_PRESSED+1
    #echo $BUTTOM_PRESSED
  else
    BUTTOM_PRESSED=0
  fi

  if [ $BUTTOM_PRESSED -ge 10 ]; then
    control_c
    exit 0
  fi

  let COUNTER=$COUNTER+1
  if [ $GET -eq 1 ] && [ $COUNTER -ge 10 ]; then
    get_time
    GET=0
    COUNTER=0
  fi

  sleep 0.1
done

exit 0
