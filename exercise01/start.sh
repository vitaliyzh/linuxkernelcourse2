#!/bin/bash 
#-x

usage ()
{
    echo "Usage: $1"
}

TEST=0
MAX_DEPTH=1
NAME_DIR=""
DIR_FOUND=0

dir_name ()
{
  cd ./$1
  dir_list=`find -maxdepth $MAX_DEPTH -name "*.tmp" -o -name "-*" -o -name "_*" -o -name "~*" `
  if [ $TEST == 1 ] ;then
    echo $dir_list
  else
     echo "unlink $dir_list"
  fi
}

help ()
{
   usage "${0##*/} {help|dir_name (dir name)}"
   exit 0;
}

if [ $# -lt  1 ]; then
    echo "Enter 2 params"
    echo "dir_name xxxx"
    exit 1
fi


case "$1" in
    dir_name)
        NAME_DIR=$2
        DIR_FOUND=1
        ;;
    *)
        help
        ;;
esac


while [ -n "$1" ] ; do
case "$1" in
-t) TEST=1 ;;
-r) MAX_DEPTH=10 ;;
-h) help ;;

esac
shift
done

if [ $DIR_FOUND==1 ]; then
  dir_name $NAME_DIR
fi
exit $RETVAL

