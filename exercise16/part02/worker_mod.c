#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/timer.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/jiffies.h>
#include <linux/types.h>

#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/jiffies.h>

#include <linux/string.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR( "Vitaliy Zhyrko vitaliyzh@gmail.com" );
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");

static int major = 0;
static int size = 5;

struct timer_list my_timer;

enum reminder_state{
    not_shown_state = 0,
    shown_state,
    undefine_state
};

struct Task
{
    char character;
    int group_length;
    int number_of_groups;
    int delay_after_printing_each_character;
    int delay_after_printing_each_group;
};

struct data {
    int n;
    bool sent;
    char job_string[1024];
    struct Task mytask;
    struct list_head list;
};

LIST_HEAD( list );

int id=0;

static ssize_t dev_write(struct file *file, const char __user *buf,
                         size_t count, loff_t *ppos);

static const struct file_operations dev_fops = {
   .owner = THIS_MODULE,
   .write  = dev_write,
};

#define DEVICE_FIRST  0
#define DEVICE_COUNT  1
#define MODNAME "my_workerdev_mod"

static struct cdev hcdev;
static struct class *devclass;

#define MAX_SIZE 1024
int buf_size = MAX_SIZE;

static char msg_str[MAX_SIZE+1];
static int msg_len = 256;

struct Worker
{
    struct task_struct *etx_thread;
    bool job_exist;
    int id;
    struct Task mytask;
};

#define WORKER_COUNT 2
struct Worker workers[WORKER_COUNT];


void ParseMsg(char *job_string, int len, struct Task *task)
{
    char *ach;
    int pos=0;

    if(job_string && len>0 && task)
    {
        char *istr;
        int param_num=0;

        bool valid=true;
        printk("Parse job: %s\n", job_string);
        while( (istr = strsep(&job_string,",\0")) != NULL )
        {
            switch(param_num)
            {
                case 0:
                {
                    printk("step %d \'%c\'", param_num, istr[0]);
                    task->character=istr[0];
                    if(task->character>='A'&&task->character<='Z')
                        valid = false;
                    break;
                }
                case 1:
                {
                    kstrtoint(istr, 10, &task->group_length);
                    printk("step %d %d", param_num, task->group_length);
                    if(task->group_length>10)
                        valid = false;
                    break;
                }
                case 2:
                {
                    kstrtoint(istr, 10, &task->number_of_groups);
                    printk("step %d %d", param_num, task->number_of_groups);
                    if(task->number_of_groups>10)
                        valid = false;
                    break;
                }
                case 3:
                {
                    kstrtoint(istr, 10, &task->delay_after_printing_each_character);
                    printk("step %d %d", param_num, task->delay_after_printing_each_character);
                    if(task->delay_after_printing_each_character>10)
                        valid = false;
                    break;
                }
                case 4:
                {
                    kstrtoint(istr, 10, &task->delay_after_printing_each_group);
                    printk("step %d %d", param_num, task->delay_after_printing_each_group);
                    if(task->delay_after_printing_each_group>10)
                        valid = false;
                    break;
                }
                default:
                {
                    printk("step %d", param_num);
                    break;
                }
            }
            if(param_num++==5)
                break;
        }
        printk("Parse job status: %s, %d", (param_num==5)?"OK":"Error", valid);
        if(param_num==5 && valid)
        {
        }
    }
}


static void check_list(void)
{
    struct list_head *iter, *iter_safe;
    struct data *item;
    list_for_each( iter, &list )
    {
       item = list_entry( iter, struct data, list );
//       if( item->time < jiffies && item->state==not_shown_state)
//       {
//            item->state = shown_state;
//            printk( KERN_INFO "Msg: [LIST] %d, [USER] %s, [MSG] %s, [TIME] %d<(%d)\n", item->n, item->user_name, item->message, item->time, jiffies );
//       }
    }
}

void add_job(const char *msg)
{
    struct data *item;
    id++;
    item = kmalloc( sizeof(*item), GFP_KERNEL );
    item->n = id;
    item->sent = false;
    snprintf(item->job_string, sizeof(item->job_string), "%s", msg);
    ParseMsg(item->job_string, strlen(item->job_string), &(item->mytask));
    list_add( &(item->list), &list );
    printk( KERN_INFO "Add: [ID] %d, [JOB] %s\n", item->n, item->job_string);
}

void delete_job(int idr)
{
    struct list_head *iter, *iter_safe;
    struct data *item;
    bool found=false;
    list_for_each( iter, &list ) {
       item = list_entry( iter, struct data, list );
       if(idr==item->n)
       {
            found=true;
            printk( KERN_INFO "Delete: [LIST] %d, [MSG] %s\n", item->n, item->job_string );
            list_del( iter );
            kfree( item );
            break;
       }
    }
    if(!found)
    {
         printk( KERN_INFO "Delete: Not Found [ID] %d", idr);
    }
}

void free_lists( void )
{
    struct list_head *iter, *iter_safe;
    struct data *item;
    list_for_each_safe( iter, iter_safe, &list ) {
       item = list_entry( iter, struct data, list );
       list_del( iter );
       kfree( item );
    }
}

void show_list(void)
{
    struct list_head *iter, *iter_safe;
    struct data *item;
    list_for_each( iter, &list ) {
       item = list_entry( iter, struct data, list );
       printk( KERN_INFO "Show: [LIST] %d, [JOB] %s\n", item->n, item->job_string );
    }
}

void test_lists( void )
{
   struct list_head *iter, *iter_safe;
   struct data *item;
   int i;
   printk( KERN_INFO "start --------------------------------->");
   for( i = 0; i < size; i++ ) {
      item = kmalloc( sizeof(*item), GFP_KERNEL );
      if( !item ) break;
      id++;
      item->n = id;
      snprintf(item->job_string, sizeof(item->job_string), "A,2,6,2,5");
      list_add( &(item->list), &list );
   }
   printk( KERN_INFO "stop --------------------------------->");
   return;
}

void fin_lists( void )
{
    free_lists();
    printk( KERN_INFO "free --------------------------------->");
    show_list();
}

static ssize_t dev_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos)
{
//    file_priv_data *data = file->private_data;

    if(copy_from_user(msg_str + *ppos, buf, count))
    {
        return -EFAULT;
    }

    //printk(KERN_INFO "write() ppos=%lld, count=%zd\n", *ppos, count);
    *ppos += count;

    if(*ppos >= msg_len-1)
    {
        *ppos = msg_len;
    }
    msg_str[*ppos+1]='\0';

    add_job(msg_str);
    return count;
}

static int dev_init( void ) {
   int ret=0,i=0;
   dev_t dev;
   memset(msg_str, 0, sizeof(msg_str));

   if( major != 0 ) {
      dev = MKDEV( major, DEVICE_FIRST );
      ret = register_chrdev_region( dev, DEVICE_COUNT, MODNAME );
   }
   else {
      ret = alloc_chrdev_region( &dev, DEVICE_FIRST, DEVICE_COUNT, MODNAME );
      major = MAJOR( dev );  // не забыть зафиксировать!
   }
   if( ret < 0 ) {
      printk( KERN_ERR "=== Can not register char device region\n" );
      goto err;
   }
   cdev_init( &hcdev, &dev_fops );
   hcdev.owner = THIS_MODULE;
   ret = cdev_add( &hcdev, dev, DEVICE_COUNT );
   if( ret < 0 ) {
      unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
      printk( KERN_ERR "=== Can not add char device\n" );
      goto err;
   }
   devclass = class_create( THIS_MODULE, "worker_class" ); /* struct class* */
   /*for( i = 0; i < DEVICE_COUNT; i++ ) */{
#define DEVNAME "worker"
      dev = MKDEV( major, DEVICE_FIRST + i );
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,26)
/* struct device *device_create( struct class *cls, struct device *parent,
                                 dev_t devt, const char *fmt, ...); */
      device_create( devclass, NULL, dev, "%s_%d", DEVNAME, i );
#else
/* struct device *device_create( struct class *cls, struct device *parent,
                                 dev_t devt, void *drvdata, const char *fmt, ...); */
      device_create( devclass, NULL, dev, NULL, "%s", DEVNAME );
#endif
   }
   printk( KERN_INFO "======== module installed %d:[%d-%d] ===========\n",
           MAJOR( dev ), DEVICE_FIRST, MINOR( dev ) );
err:
   return ret;
}

static void dev_exit( void )
{
   dev_t dev;
   int i;
   for( i = 0; i < DEVICE_COUNT; i++ ) {
      dev = MKDEV( major, DEVICE_FIRST + i );
      device_destroy( devclass, dev );
   }
   class_destroy( devclass );
   cdev_del( &hcdev );
   unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
   printk( KERN_INFO "=============== module removed ==================\n" );
}

///timer
///
static void check_list2(void)
{
    struct list_head *iter, *iter_safe;
    struct data *item;
    bool shown_all = true;
    int i;
    list_for_each( iter, &list )
    {
        item = list_entry( iter, struct data, list );
        if(!item->sent)
        {
            for(i=0;i<WORKER_COUNT;i++)
            {
                if(workers[i].job_exist==false)
                {
                    workers[i].mytask = item->mytask;
                    workers[i].job_exist=true;

                    printk("Transfer job k %d, status job %d\n", i, workers[i].job_exist);
                }
                else
                {
                    //printk("Buzy k %d\n", i);
                }
            }
        }
    }
}

static void timer_function(struct timer_list *tl)
{
    struct timer_list * p = tl;
    check_list2();
    mod_timer(p, p->expires + HZ/2);
}

static int timer_init(void)
{
    printk("My Timer: start\n");
    timer_setup(&my_timer, timer_function, 0);
    mod_timer(&my_timer, jiffies + HZ*3);
    return 0;
}

static void timer_exit(void)
{
    del_timer(&my_timer);
    printk("My Timer: stop\n");
}

int thread_function(void *pv)
{
    //struct Worker worker = *(struct Worker*)pv;
    int id = *(int*)pv;
    int i,j,k;
    while(!kthread_should_stop())
    {
        printk("T %d: job %s", workers[id].id, (workers[id].job_exist==true)?"exist":"empty");

        if(workers[id].job_exist)
        {
            printk("T %d Start job", workers[id].id);

            #define BUFFER_SIZE 128
            char buffer[BUFFER_SIZE+1];
            int pos=0;
            for(k=0; k<workers[id].mytask.number_of_groups;k++)
            {
                for(i=0; i<workers[id].mytask.group_length;i++)
                {
                    if(pos<BUFFER_SIZE)
                    {
                        buffer[pos]=workers[id].mytask.character;
                        pos++;
                    }
                    else
                        break;
                    for(j=0; j<workers[id].mytask.delay_after_printing_each_character; j++)
                    {
                        if(pos<BUFFER_SIZE)
                        {
                            buffer[pos]=' ';
                            pos++;
                        }
                        else
                            break;
                    }
                }
                for(j=0; j<workers[id].mytask.delay_after_printing_each_group; j++)
                {
                    if(pos<BUFFER_SIZE)
                    {
                        buffer[pos]=' ';
                        pos++;
                    }
                    else
                        break;
                }
            }
            buffer[pos]='\0';
            printk( KERN_INFO "%s", buffer);
            workers[id].job_exist=false;
        }
        msleep(1000);
    }
    return 0;
}

void StartThread(void)
{
    int i;
    for(i=0;i<WORKER_COUNT;i++)
    {
        workers[i].id=i;
        workers[i].job_exist=false;
        workers[i].etx_thread = kthread_create(thread_function, &workers[i].id, "eTx Thread");
        if(workers[i].etx_thread)
        {
            wake_up_process(workers[i].etx_thread);
        }
        else
        {
            pr_err("Cannot create kthread %d\n", i);
            return;
        }
    }
}

void StopThread(void)
{
    int i;
    for(i=0;i<WORKER_COUNT;i++)
    {
        if(workers[i].etx_thread)
            kthread_stop(workers[i].etx_thread);
    }
}

static int __init mod_init( void )
{
    if(dev_init()!=0)
        return -1;

    test_lists();
    StartThread();
    timer_init();
    return 0;
}

static void __exit mod_exit(void)
{
    dev_exit();
    timer_exit();
    StopThread();
    fin_lists();
}

module_init( mod_init );
module_exit( mod_exit );
