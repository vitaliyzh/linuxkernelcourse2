Exercise #16. Concurrency and Synchronization∗

Description∗
    Working with the fixed thread pool.

Guidance∗
    Use Git and the following names: exercise16 - for your project's home directory; partXX - for the directory of each subtask (XX - subtask number); src - for the source code directory.

Subtask #1. Implement the following task as a user-space application.

    1⃣ You have a fixed size pool of executor threads.

    2⃣ The master thread receives jobs for execution via stdin/sysfs/procfs and assigns them to executors.

    3⃣ Each executor thread does its job: it prints a group of repeating characters several times.

Subtask #2. Implement the same task as a kernel module.

    Job parameters

        character for printing
        group length
        number of groups
        delay after printing each character
        delay after printing each group
