#include <stdio.h>
#include <stdlib.h>
#include "worker.h"

//https://prog-cpp.ru/data-ols/
struct list
{
    struct Task task;
    struct list *ptr;
};

struct list * init(struct Task newtask)
{
    struct list *lst;
    lst = (struct list*)malloc(sizeof(struct list));
    lst->task = newtask;
    lst->ptr = NULL;
    return(lst);
}

struct list * addelem(struct list *lst, struct Task newtask)
{
    struct list *temp, *p;
    temp = (struct list*)malloc(sizeof(struct list));
    p = lst->ptr;
    lst->ptr = temp;
    temp->task = newtask;
    temp->ptr = p;
    return(temp);
}

struct list * deletelem(struct list *lst, struct list *root)
{
    struct list *temp;
    temp = root;
    while (temp->ptr != lst)
    {
        temp = temp->ptr;
    }
    temp->ptr = lst->ptr;
    free(lst);
    return(temp);
}

struct list * deletehead(struct list *root)
{
  struct list *temp;
  temp = root->ptr;
  free(root);
  return(temp);
}

void listprint(struct list *lst)
{
      struct list *p;
      p = lst;
      do {
        printf("task %c[count %d]+[space after char %d], \n groups count %d + space after group - %d ",  p->task.character,
                                        p->task.group_length, p->task.delay_after_printing_each_character,
                                        p->task.number_of_groups, p->task.delay_after_printing_each_group);
        p = p->ptr;
      } while (p != NULL);
}

void AddTask()
{

}

int main(void)
{
    int ret = 0;
    InitWorkers();

    struct Task task[10] = {{'A',2,6,2,5},
                            {'B',2,6,2,5},
                            {'C',2,6,2,5},
                            {'D',2,6,2,5},
                            {'E',2,6,2,5},
                            {'F',2,6,2,5},
                            {'G',2,6,2,5},
                            {'H',2,6,2,5},
                            {'I',2,6,2,5},
                            {'J',2,6,2,5}};

    for (int i=0; i<10;i++)
    {
        while(1)
        {
            int id = GetFreeWorker();

            if(id!=-1)
            {
                printf("Send task\n");
                AddWork(id, &task[i]);
                break;
            }
        }
    }

    sleep(1);
    FreeWorkers();
    return ret;
}
