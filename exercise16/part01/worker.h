#ifndef WORKER
#define WORKER

#include <pthread.h>
#include <stdbool.h>

#define WorkerCount 3

enum WorkerStatus
{
    WorkerFree = 0,
    WorkerBuzy,
    WorkerStopped,
    WorkerEmpty
};

struct Task
{
//    Task(char a, int g, int n, int d1, int d2)
//    {
//    }
    char character ;
    int group_length;
    int number_of_groups;
    int delay_after_printing_each_character;
    int delay_after_printing_each_group;
};

struct Worker
{
    int id;
    int status;
    bool stoping;
    bool exist_work;
    pthread_t pthread;
    struct Task task;
};

struct Worker Workers[WorkerCount];

void InitWorkers();
void FreeWorkers();
int GetFreeWorker();
int AddWork(int id, struct Task *task);

#endif //WORKER
