#include "worker.h"
#include <unistd.h>
#include <stdio.h>

void * execute_worker(void *param)
{
    struct Worker *pWorker = (struct Worker *)param;
    if(pWorker)
    {
        printf("Worker[%d]: Thread start.\n", pWorker->id);
        while(!pWorker->stoping)
        {
            if(pWorker->exist_work)
            {
                #define BUFFER_SIZE 128
                char buffer[BUFFER_SIZE+1];
                int pos=0;
                for(int k=0; k<pWorker->task.number_of_groups;k++)
                {
                    for(int i=0; i<pWorker->task.group_length;i++)
                    {
                        if(pos<BUFFER_SIZE)
                        {
                            buffer[pos]=pWorker->task.character;
                            pos++;
                        }
                        else
                            break;
                        for(int j=0; j<pWorker->task.delay_after_printing_each_character; j++)
                        {
                            if(pos<BUFFER_SIZE)
                            {
                                buffer[pos]=' ';
                                pos++;
                            }
                            else
                                break;
                        }
                    }
                    for(int j=0; j<pWorker->task.delay_after_printing_each_group; j++)
                    {
                        if(pos<BUFFER_SIZE)
                        {
                            buffer[pos]=' ';
                            pos++;
                        }
                        else
                            break;
                    }
                }
                buffer[pos]='\0';

                printf("%s\n", buffer);

                pWorker->exist_work = false;
                pWorker->status = WorkerFree;
            }
            usleep(10);
        }
        pWorker->status = WorkerStopped;
        printf("Worker[%d]: Thread stop.\n", pWorker->id);
    }
    return NULL;
}

void InitWorkers()
{
    int i, ret;
    for(int i = 0; i<WorkerCount; i++)
    {
       Workers[i].id = i;
       Workers[i].status = WorkerEmpty;
       Workers[i].stoping = false;
       ret = pthread_create (&Workers[i].pthread, NULL, &execute_worker, &Workers[i]);
       if (ret < 0)
       {
           printf("Worker[%d]: pthread creation failed.\n", i);
       }
       else
       {
           printf("Worker[%d]: Thread starting.\n", i);
           Workers[i].status = WorkerFree;
           pthread_detach(Workers[i].pthread);

       }
    }
}

void FreeWorkers()
{
    for(int i = 0; i<WorkerCount; i++)
    {
        printf("Worker[%d]: Thread stopping.\n", Workers[i].id);
        Workers[i].stoping = true;
        //pthread_cancel(CopyThread);
        pthread_join(Workers[i].pthread, NULL);
        Workers[i].pthread=0;
    }
}

int GetFreeWorker()
{
    int ret = -1;
    for(int i = 0; i<WorkerCount; i++)
    {
        if(Workers[i].pthread && Workers[i].status == WorkerFree)
        {
            ret = i;
            break;
        }
        else
            printf("Buzy %d\n", i);
    }
    return ret;
}

int AddWork(int id, struct Task *task)
{
    int ret = -1;
    if( task && id >= 0 && id < WorkerCount && Workers[id].pthread && !Workers[id].exist_work)
    {
        printf("Worker[%d]: Get task\n", id);
        Workers[id].task = *task;
        Workers[id].exist_work = true;
        Workers[id].status = WorkerBuzy;
        ret = 0;
    }
    return ret;
}
