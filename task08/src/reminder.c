#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/timer.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/jiffies.h>
#include <linux/types.h>

#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR( "Vitaliy Zhyrko vitaliyzh@gmail.com" );
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");

static int major = 0;

static int size = 5;
module_param( size, int, S_IRUGO | S_IWUSR );

struct timer_list my_timer;

enum reminder_state{
    not_shown_state = 0,
    shown_state,
    undefine_state
};

struct data {
    int n;
    unsigned long time;
    char user_name[32];
    char message[1024];
    int state;
    struct list_head list;
};

LIST_HEAD( list );

int id=0;

static ssize_t dev_write(struct file *file, const char __user *buf,
                         size_t count, loff_t *ppos);

static const struct file_operations dev_fops = {
   .owner = THIS_MODULE,
   .write  = dev_write,
};

#define DEVICE_FIRST  0
#define DEVICE_COUNT  1
#define MODNAME "my_msgdev_mod"

static struct cdev hcdev;
static struct class *devclass;

#define MAX_SIZE 1024
int buf_size = MAX_SIZE;

static char msg_str[MAX_SIZE+1];
static int msg_len = 256;

void ParseMsg(char *msg, int len);

static ssize_t dev_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos)
{
//    file_priv_data *data = file->private_data;

    if(copy_from_user(msg_str + *ppos, buf, count))
    {
        return -EFAULT;
    }

    printk(KERN_INFO "write() ppos=%lld, count=%zd\n", *ppos, count);
    *ppos += count;

    if(*ppos >= msg_len-1)
    {
        *ppos = msg_len;
    }
    msg_str[*ppos]='\n';
    msg_str[*ppos+1]='\0';

    ParseMsg(msg_str, count);
    return count;
}

static int dev_init( void ) {
   int ret=0, i, j;
   dev_t dev;
   memset(msg_str, 0, sizeof(msg_str));

   if( major != 0 ) {
      dev = MKDEV( major, DEVICE_FIRST );
      ret = register_chrdev_region( dev, DEVICE_COUNT, MODNAME );
   }
   else {
      ret = alloc_chrdev_region( &dev, DEVICE_FIRST, DEVICE_COUNT, MODNAME );
      major = MAJOR( dev );  // не забыть зафиксировать!
   }
   if( ret < 0 ) {
      printk( KERN_ERR "=== Can not register char device region\n" );
      goto err;
   }
   cdev_init( &hcdev, &dev_fops );
   hcdev.owner = THIS_MODULE;
   ret = cdev_add( &hcdev, dev, DEVICE_COUNT );
   if( ret < 0 ) {
      unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
      printk( KERN_ERR "=== Can not add char device\n" );
      goto err;
   }
   devclass = class_create( THIS_MODULE, "msg_class" ); /* struct class* */
   /*for( i = 0; i < DEVICE_COUNT; i++ ) */{
#define DEVNAME "msg"
      dev = MKDEV( major, DEVICE_FIRST + i );
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,26)
/* struct device *device_create( struct class *cls, struct device *parent,
                                 dev_t devt, const char *fmt, ...); */
      device_create( devclass, NULL, dev, "%s_%d", DEVNAME, i );
#else
/* struct device *device_create( struct class *cls, struct device *parent,
                                 dev_t devt, void *drvdata, const char *fmt, ...); */
      device_create( devclass, NULL, dev, NULL, "%s", DEVNAME );
#endif
   }
   printk( KERN_INFO "======== module installed %d:[%d-%d] ===========\n",
           MAJOR( dev ), DEVICE_FIRST, MINOR( dev ) );
err:
   return ret;
}

static void dev_exit( void )
{
   dev_t dev;
   int i;
   for( i = 0; i < DEVICE_COUNT; i++ ) {
      dev = MKDEV( major, DEVICE_FIRST + i );
      device_destroy( devclass, dev );
   }
   class_destroy( devclass );
   cdev_del( &hcdev );
   unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
   printk( KERN_INFO "=============== module removed ==================\n" );
}


static void show_all_reminder_for_user(char *name)
{
    printk("Show: for %s", name);
    struct list_head *iter, *iter_safe;
    struct data *item;
    bool show_for_all=false;
    if(!strcmp(name, "all"))
    {
        show_for_all = true;
    }
    list_for_each( iter, &list )
    {
       item = list_entry( iter, struct data, list );
       if( show_for_all || !strcmp(item->user_name, name))
       {
            printk( KERN_INFO "Msg: [LIST] %d, [USER] %s, [MSG] %s, [TIME] %d, [STATE] %s\n", item->n, item->user_name, item->message, item->time,
                    (item->state==not_shown_state ? "NOT SHOWN" : "SHOWN"));
       }
       else
       {
           //printk( KERN_INFO "Msg: [TIME] %d<(%d)\n", item->time, jiffies );
       }
    }
}

static void check_list(void)
{
    //printk("Check: tick %d", jiffies);
    struct list_head *iter, *iter_safe;
    struct data *item;
    bool shown_all = true;
    list_for_each( iter, &list )
    {
       item = list_entry( iter, struct data, list );
       if( item->time < jiffies && item->state==not_shown_state)
       {
            item->state = shown_state;
            printk( KERN_INFO "Msg: [LIST] %d, [USER] %s, [MSG] %s, [TIME] %d<(%d)\n", item->n, item->user_name, item->message, item->time, jiffies );
       }
       else
       {
           //printk( KERN_INFO "Msg: [TIME] %d<(%d)\n", item->time, jiffies );
       }

       if( item->state==not_shown_state)
       {
           shown_all = false;
       }
    }
//    if(shown_all)
//    {
//        show_all_reminder_for_user("all");
//    }
}

void add_user_remine(const char *name, const char *msg, const u32 delay)
{
    struct data *item;
    id++;
    item = kmalloc( sizeof(*item), GFP_KERNEL );
    item->n = id;
    item->time = jiffies+(delay*HZ);
    item->state = not_shown_state;
    snprintf(item->user_name, sizeof(item->user_name), "%s", name);
    snprintf(item->message, sizeof(item->message), "%s", msg);
    list_add( &(item->list), &list );
    printk( KERN_INFO "Add: [LIST] %d, [USER] %s, [MSG] %s, [TIME] %d\n", item->n, item->user_name, item->message, item->time );
}

void delete_user(const char *name, int idr)
{
    struct list_head *iter, *iter_safe;
    struct data *item;
    bool found=false;
    list_for_each( iter, &list ) {
       item = list_entry( iter, struct data, list );
       if(!strcmp(item->user_name, name)&&idr==item->n)
       {
            found=true;
            printk( KERN_INFO "Delete: [LIST] %d, [USER] %s, [MSG] %s\n", item->n, item->user_name, item->message );
            list_del( iter );
            kfree( item );
            break;
       }
    }
    if(!found)
    {
         printk( KERN_INFO "Delete: Not Found [USER] %s", name);
    }
}

void find_user(const char *name)
{
    struct list_head *iter, *iter_safe;
    struct data *item;
    bool found=false;
    list_for_each( iter, &list ) {
       item = list_entry( iter, struct data, list );
       if(!strcmp(item->user_name, name))
       {
           found=true;
           printk( KERN_INFO "Find: Found [LIST] %d, [USER] %s, [MSG] %s\n", item->n, item->user_name, item->message );
       }
    }
    if(!found)
    {
         printk( KERN_INFO "Find: Not Found [USER] %s", name);
    }
}

void free_lists( void )
{
    struct list_head *iter, *iter_safe;
    struct data *item;
    list_for_each_safe( iter, iter_safe, &list ) {
       item = list_entry( iter, struct data, list );
       list_del( iter );
       kfree( item );
    }
}

void show_list(void)
{
    struct list_head *iter, *iter_safe;
    struct data *item;
    list_for_each( iter, &list ) {
       item = list_entry( iter, struct data, list );
       printk( KERN_INFO "Show: [LIST] %d, [USER] %s, [MSG] %s, [TIME] %d\n", item->n, item->user_name, item->message, item->time );
    }
}

void test_lists( void )
{
   struct list_head *iter, *iter_safe;
   struct data *item;
   int i;
//   LIST_HEAD( list );
   printk( KERN_INFO "start --------------------------------->");
   for( i = 0; i < size; i++ ) {
      item = kmalloc( sizeof(*item), GFP_KERNEL );
      if( !item ) goto out;
      id++;
      item->n = id;
      item->time = jiffies+id*HZ*2;
      item->state = 0;
      snprintf(item->user_name, sizeof(item->user_name), "user%d", id);
      snprintf(item->message, sizeof(item->message), "message%d", id);
      list_add( &(item->list), &list );
   }

   add_user_remine("user77", "message77", 15);
   add_user_remine("user77", "message771", 10);
   add_user_remine("user77", "message772", 12);
   add_user_remine("user77", "message773", 13);
   add_user_remine("user77", "message774", 14);
   add_user_remine("user77", "message775", 15);
   add_user_remine("user77", "message776", 16);
   add_user_remine("user77", "message777", 17);
   add_user_remine("user77", "message778", 18);
   add_user_remine("user77", "message779", 19);

   printk( KERN_INFO "add --------------------------------->");
   find_user("user3");
   find_user("user88");
   printk( KERN_INFO "find --------------------------------->");
   show_list();
//   list_for_each( iter, &list ) {
//      item = list_entry( iter, struct data, list );
//      printk( KERN_INFO "[LIST] %d, [USER] %s, [MSG] %s\n", item->n, item->user_name, item->message );
//   }
   delete_user("user77", 0);
   delete_user("user77", 7);
   printk( KERN_INFO "del --------------------------------->");
   show_list();
//   list_for_each( iter, &list ) {
//      item = list_entry( iter, struct data, list );
//      printk( KERN_INFO "[LIST] %d, [USER] %s, [MSG] %s\n", item->n, item->user_name, item->message );
//   }
   //show_all_reminder_for_user("all");
   //show_all_reminder_for_user("user5");
out:
#if 0
    free_lists();
//   list_for_each_safe( iter, iter_safe, &list ) {
//      item = list_entry( iter, struct data, list );
//      list_del( iter );
//      kfree( item );
//   }

   printk( KERN_INFO "free --------------------------------->");
   show_list();
//   list_for_each( iter, &list ) {
//      item = list_entry( iter, struct data, list );
//      printk( KERN_INFO "[LIST] %d, [USER] %s, [MSG] %s\n", item->n, item->user_name, item->message );
//   }
#endif
   printk( KERN_INFO "end --------------------------------->");
   printk( KERN_INFO "");
}

void fin_lists( void )
{
    free_lists();
    printk( KERN_INFO "free --------------------------------->");
    show_list();
}

#include <linux/string.h>
void ParseMsg(char *msg, int len)
{
    char *ach;
    int pos=0;

    if(msg && len>0)
    {
        char *istr;
        int param_num=0;
        int cmd=0;
        char user[msg_len];
        char umsg[msg_len];
        int time;
        int number;
        bool valid=false;
        printk("Parse msg: %s\n", msg);
        while( (istr = strsep(&msg,":\n\0")) != NULL )
        {
            switch(param_num)
            {
                case 0:
                {
                    if(!strcmp(istr, "get"))
                        cmd=1;
                    else if(!strcmp(istr, "set"))
                        cmd=2;
                    else if(!strcmp(istr, "clr"))
                        cmd=3;
                    break;
                }
                case 1:
                {
                    strncpy(user, istr, sizeof(user));
                    if(cmd==1||cmd==2||cmd==3)
                    {
                        valid = true;
                    }
                    break;
                }
                case 2:
                {
                    if(cmd==2)
                    {
                        strncpy(umsg, istr, sizeof(umsg));
                    }
                    else if(cmd==3)
                    {
                        if ( kstrtoint(istr, 10, &number))
                        {
                            valid = true;
                        }
                    }
                    break;
                }
                case 3:
                {
                    if ( kstrtoint(istr, 10, &time))
                    {
                        valid = true;
                    }
                    break;
                }
            }

            //printk("%s\n", istr);
            param_num++;
        }
        if(valid)
        {
            switch(cmd)
            {
                case 1:
                {
                    printk("CMD: get %d, user: %s", cmd, user);
                    show_all_reminder_for_user(user);
                    break;
                }
                case 2:
                {
                    printk("CMD: set %d, user: %s, msg: %s, time: %d", cmd, user, umsg, time);
                    add_user_remine(user, umsg, time);
                    break;
                }
                case 3:
                {
                    printk("CMD: clr %d, user: %s, number: %d", cmd, user, number);
                    delete_user(user, number);
                    break;
                }
            }
        }
    }
}

///timer
static void timer_function(struct timer_list *tl)
{
    struct timer_list * p = tl;
    check_list();
    mod_timer(p, p->expires + HZ/2);
}

static int timer_init(void)
{
    printk("My Timer: start\n");
    timer_setup(&my_timer, timer_function, 0);
    mod_timer(&my_timer, jiffies + HZ/2);
    return 0;
}

static void timer_exit(void)
{
    del_timer(&my_timer);
    printk("My Timer: stop\n");
}

static int __init mod_init( void )
{
    if(dev_init()!=0)
        return -1;
    timer_init();
    test_lists();
    return 0;
}

static void __exit mod_exit(void)
{
    timer_exit();
    fin_lists();
    dev_exit();
}

module_init( mod_init );
module_exit( mod_exit );
