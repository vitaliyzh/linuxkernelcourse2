Task #8. Memory Management∗
Description∗
Multi-user reminders.

Guidance∗
Use Git and the following names: task08 - for your project's home directory; partXX - for the directory of each subtask (XX - subtask number); src - for the source code directory.

Create a kernel module to provide the following functionality.

1⃣ Set a new reminder. Specify user name, message text and delay in seconds relative to the current moment.

2⃣ Get reminders for the current user. Specify user name.

3⃣ Get a list of all reminders for all users.

4⃣ Cancel a previously set reminder. Specify user name and reminder number.

Extra∗
Use lists.
Use no more than one timer.
