Exercise #9. Virtual file system. Programming interface. Resources.

1) Install the Linux Kernel headers.
2) Subtask #1. Build and test ex01 module:
3) Subtask #2. Fix bugs, build and test modules ex02 and test_ex02:
4) Implement a goal called test.
5) Build the previously created kernel modules for Linux deployed with Qemu. Install and remove them (see: Secure Copy).



Result 5 subtask

vitaliy@vitaliy-HP-ZBook-15-G5:~/projects/LinuxKernelCourse2.0Participants-summer2021/linuxkernelcourse2/exercise09$ make -f ./Makefile2 test
echo 

sshpass -p pass scp -P 8022 *.ko user@127.0.0.1:
ssh -i ./ssh/id_rsa_recorder -o StrictHostKeyChecking=no -p 8022 user@127.0.0.1 "echo Hello:; ls;"
Hello:
ex01.ko
ex02.ko
test_02.ko
ssh -i ./ssh/id_rsa_recorder -o StrictHostKeyChecking=no -p 8022 root@127.0.0.1 "dmesg -c; cd /home/user;  insmod ./ex01.ko; insmod ./ex02.ko; insmod ./test_02.ko; lsmod; rmmod ./ex01.ko; rmmod ./ex02.ko; lsmod; dmesg"
root@127.0.0.1's password: 
insmod: ERROR: could not insert module ./test_02.ko: Operation not permitted
Module                  Size  Used by
ex02                   16384  0
ex01                   16384  0
Module                  Size  Used by
[  783.616875] Hello!!!
[  783.739084] ex02: Hello!!!
[  783.739347] ex02: this string returned from test2_01
[  783.739694] ex02: this string returned from test2_02
[  783.775209] test_ex02: Hello!!!
[  783.775392] test_ex02: this string returned from test2_01
[  783.775502] test_ex02: this string returned from test2_02
[  783.775751] test_ex02: Bye...
[  783.939208] Bye...
[  783.973452] ex02: Bye...
ssh -i ./ssh/id_rsa_recorder -o StrictHostKeyChecking=no -p 8022 user@127.0.0.1 "echo Remove:; rm *.ko; ls; echo Exit:"
Remove:
Exit:
