Exercise #10. Virtual file system. Programming interface. Resources.

1) Subtask #1. Build and test Procfs demo module.

2) Subtask #2. Develop a kernel module:

    to flip all words in a string;
    to make a string uppercase.
    
3) Subtask #3. Add the interfaces into the module:

    procfs to store/display user string.
    sysfs to select the method of translation:
    0 - no translation;
    1 - flip;
    2 - uppercase.

Extra∗
Implement bash script for the module testing.
Provide a test scenario.
