# Author

Vitaliy Zhyrko
Kiev, Ukraine

# Title

Parrot cockatoo - Open mic with noise canceling, LCD screen and playback to speaker

# Description

1. Purpose of development.

Using the acquired knowledge on the course, develop a prototype of future projects for smart home:
    ip intercom with door opening control
    sip phone with a touch screen
    ip cam with face detect
    cloud storage of recorded information

2. Technical equipment.
    Raspberry Pi Zero W V1.1
    micro SDHC 16GB
    1.8" 128х160 TFT LCD ST7735
    I2S MEMS Microphone SPH0645LM4H
    I2S 3W MAX98357A Amplifier with speaker 3W

## References

1. [Video of ...](URL_of_an_external_resource).
2. [Photo of ...](URL_of_an_external_resource).
3. Description: https://docs.google.com/document/d/19oy5qJWod6LBkfk-SHc6Ti-zbBXrTzVHiu_ixtNh390/edit?usp=sharing


