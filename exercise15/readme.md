Exercise #15. Interrupts Handling∗

Description∗
    Implement a counter using an encoder.

References∗
    Interrupts handling examples.
    Encoder example.
    Guidance∗
    Use Git and the following names: exercise14 - for your project's home directory; partXX - for the directory of each subtask (XX - subtask number); src - for the source code directory.

The counter can take positive and negative values.

    Turn the encoder to the left to decrement the counter.
    Turn the encoder to the right to increment the counter.
    
Extra∗
Use procfs/sysfs interfaces:

    to read and set/reset the counter;
    to reverse the direction of increasing or decreasing the counter.
