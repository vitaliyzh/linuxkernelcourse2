#/bin/bash

sudo dmesg -C
echo -e "\nmod_ser" | sudo tee /dev/kmsg
sudo insmod ./mod_ser.ko
sudo rmmod mod_ser

echo -e "\nmod_tasklet" | sudo tee /dev/kmsg
sudo insmod ./mod_tasklet.ko
sudo rmmod mod_tasklet

echo -e "\nmod_workqueue" | sudo tee /dev/kmsg
sudo insmod ./mod_workqueue.ko
sudo rmmod mod_workqueue

echo -e "\nlab1_interrupt" | sudo tee /dev/kmsg
sudo insmod ./lab1_interrupt.ko
sudo rmmod lab1_interrupt


sudo dmesg

