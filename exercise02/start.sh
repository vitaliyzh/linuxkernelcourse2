#!/bin/bash 
#-x

usage ()
{
    echo "Usage: $1"
}

NAME_DIR=""
DIR_FOUND=0

dir_name ()
{
  TIME_NOW=`date +%s`
  let ONE_MONTH=60*60*24*31
  ONCE=0
  echo time now: $TIME_NOW, one_month: $ONE_MONTH

  cd ./$1
  file_list=`find -name "*.tmp" -o -name "-*" -o -name "_*" -o -name "~*" `

   for entry in $file_list; do
	TIME_FILE=`stat --printf=%Y $entry`
	#TIME_DELTA=${TIME_NOW-TIME_FILE}
	let TIME_DELTA=$TIME_NOW-$TIME_FILE
	if [ $TIME_DELTA -lt $ONE_MONTH ]; then RENAME_FILE=1; else RENAME_FILE=0; fi
	echo $entry: $TIME_FILE   time_delta: $TIME_DELTA  `if [ $TIME_DELTA -lt $ONE_MONTH ]; then echo "LESS"; else echo "MORE"; fi`
	
	#echo RENAME: $RENAME_FILE
	#echo path=${entry%/*}/
	if [ $RENAME_FILE = 1 ]; then echo "mv $entry ${entry%/*}/~$(basename $entry)  "; ONCE=1; fi
   done

    if [ $ONCE = 1 ]; then
	../exercise01/start.sh $1
	#run ex01
    fi
}

help ()
{
   usage "${0##*/} {help|dir_name (dir name)}"
   exit 0;
}

if [ $# -lt  1 ]; then
    echo "Enter 1 params"
    echo "dir_name xxxx"
    exit 1
fi


case "$1" in
    dir_name)
        NAME_DIR=$2
        DIR_FOUND=1
        ;;
    *)
        help
        ;;
esac


if [ $DIR_FOUND==1 ]; then
  dir_name $NAME_DIR
fi

exit $RETVAL

