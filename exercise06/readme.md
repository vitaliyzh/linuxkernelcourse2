Exercise #6. Data Input/Output. Serial Interfaces

Description∗
Подключить кнопку из набора GL Raspberry Kit к Raspberry Pi Zero W используя вывод GPIO26.

Написать bash-скрипт для подсчета и отображения количества нажатий на кнопку (GPIO26).

Устранить дребезг контактов кнопки, если он присутствует.

