#/bin/bash

GPIO_PATH=/sys/class/gpio

trap control_c SIGINT

control_c()
{
    echo "disable gpio26"
    echo 26 > $GPIO_PATH/unexport
    exit 0
}


init_port()
{
    echo "init port"
    if [ -e $GPIO_PATH/gpio26 ]; then
    {
       echo "enable gpio26"
       echo 26 > $GPIO_PATH/export
       sleep 1
       echo in > $GPIO_PATH/gpio26/direction
       sleep 1
    }
    fi
}

COUNTER=0
OLD_VAL=0
init_port
while [ 1 ]; do
  VAL=$(cat $GPIO_PATH/gpio26/value)
  #echo $VAL
  if [ $VAL -ne $OLD_VAL ]; then
    COUNTER=$((COUNTER+1))
    echo $COUNTER #$VAL $OLD_VAL
    OLD_VAL=$VAL
  fi
  sleep 0.1
done




