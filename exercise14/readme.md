Exercise #14. Memory management∗

Description∗
    Working with Linux memory management subsystem.

Guidance∗
    Use Git and the following names: exercise14 - for your project's home directory; partXX - for the directory of each subtask (XX - subtask number); src - for the source code directory.

1⃣ Subtask #1

    Create user-space C program which tries to allocate buffers with sizes x in range from 0 to maximum possible value using functions: malloc, calloc, alloca.
    Measure time of each allocation/freeing: allocate memory, free memory, print results, double the amount of memory until an error occurs.
    
2⃣ Subtask #2

    Create kernel module and measure allocation/freeing time for functions: kmalloc, kzmalloc, vmalloc, get_free_pages.
    Measure the time of each allocation/freeing action. The results should be presented in a table with the following columns: buffer size, allocation time, freeing time. Size unit is 1 byte, time unit is 1 ns.
