#include "dev.h"
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/version.h>      /* LINUX_VERSION_CODE */
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
//#include <asm/uaccess.h>
#include <linux/uaccess.h>

MODULE_LICENSE( "GPL" );
MODULE_AUTHOR( "Vitaliy Zhyrko <vitaliyzh@gmail.com>" );
MODULE_VERSION( "1.0" );


static int major = 0;

static int msg_len = 0;
module_param( msg_len, int, S_IRUGO );

static ssize_t dev_read( struct file * file, char * buf,
                           size_t count, loff_t *ppos );
static ssize_t dev_write(struct file *file, const char __user *buf,
                         size_t count, loff_t *ppos);

static const struct file_operations dev_fops = {
   .owner = THIS_MODULE,
   .read  = dev_read,
   .write  = dev_write,
};

#define DEVICE_FIRST  0
#define DEVICE_COUNT  1
#define MODNAME "my_msgdev_mod"

static struct cdev hcdev;
static struct class *devclass;

#define MAX_SIZE 1024
int buf_size = MAX_SIZE;

static char hello_str[MAX_SIZE+1];

#define MAX_USER 3
#define MAX_MESSAGES 128
static char *users[MAX_USER]={"user1", "user2", "user3"};
static char messages[MAX_MESSAGES][MAX_SIZE+1];
int message_in_queue=0;

static ssize_t dev_read( struct file * file, char * buf,
                           size_t count, loff_t *ppos ) {
   int j;
   bool found=false;
   char *istr=NULL;
   int len = strlen( hello_str );
   printk( KERN_INFO "=== read : %ld\n", (long)count );
   if( count < len ) return -EINVAL;
   if( *ppos != 0 ) {
      printk( KERN_INFO "=== read return : 0\n" );  // EOF
      return 0;
   }
   for(j=0;j<MAX_USER;j++)
   {
       //printk( KERN_INFO "(%d) \"%s\" in \"%s\"\n", j, users[j], hello_str );
       if((istr=strnstr(hello_str, users[j], strlen(users[j])))!=NULL)
       {
           //printk( KERN_INFO "found");
           found = true;
           break;
       }
       else {
           //printk( KERN_INFO "ret %x", istr);
       }
   }

   if(!found)
   {
       static char *no_msg_str="No Message\n";
       len = strlen( no_msg_str );
       if( copy_to_user( buf, no_msg_str, len ) ) return -EINVAL;
       *ppos = len;
       return len;
   }

   if( copy_to_user( buf, hello_str, len ) ) return -EINVAL;
   *ppos = len;
   printk( KERN_INFO "=== read return : %d\n", len );
   return len;
}

static ssize_t dev_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos)
{
//    file_priv_data *data = file->private_data;

    if(copy_from_user(hello_str + *ppos, buf, count))
    {
        return -EFAULT;
    }

    printk(KERN_INFO "write() ppos=%lld, count=%zd\n", *ppos, count);
    *ppos += count;

    if(*ppos >= msg_len)
    {
        *ppos = msg_len;
    }
    hello_str[*ppos]='\n';
    hello_str[*ppos+1]='\0';

    return count;
}


static char *msg_str = "Hello, world!\n";

static int __init dev_init( void ) {
   int ret, i, j;
   dev_t dev;
   strncpy(hello_str, msg_str, sizeof(hello_str));

   if(msg_len>MAX_SIZE)
   {
       msg_len = MAX_SIZE;
   }
   if(msg_len<=0)
   {
       msg_len = MAX_SIZE;
   }
   for(j=0;j<MAX_USER;j++)
   {
       printk(KERN_ERR "%d - %s\n", j, users[j]);
   }

   if( major != 0 ) {
      dev = MKDEV( major, DEVICE_FIRST );
      ret = register_chrdev_region( dev, DEVICE_COUNT, MODNAME );
   }
   else {
      ret = alloc_chrdev_region( &dev, DEVICE_FIRST, DEVICE_COUNT, MODNAME );
      major = MAJOR( dev );  // не забыть зафиксировать!
   }
   if( ret < 0 ) {
      printk( KERN_ERR "=== Can not register char device region\n" );
      goto err;
   }
   cdev_init( &hcdev, &dev_fops );
   hcdev.owner = THIS_MODULE;
   ret = cdev_add( &hcdev, dev, DEVICE_COUNT );
   if( ret < 0 ) {
      unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
      printk( KERN_ERR "=== Can not add char device\n" );
      goto err;
   }
   devclass = class_create( THIS_MODULE, "msg_class" ); /* struct class* */
   /*for( i = 0; i < DEVICE_COUNT; i++ ) */{
#define DEVNAME "msg"
      dev = MKDEV( major, DEVICE_FIRST + i );
#if LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,26)
/* struct device *device_create( struct class *cls, struct device *parent,
                                 dev_t devt, const char *fmt, ...); */
      device_create( devclass, NULL, dev, "%s_%d", DEVNAME, i );
#else
/* struct device *device_create( struct class *cls, struct device *parent,
                                 dev_t devt, void *drvdata, const char *fmt, ...); */
      device_create( devclass, NULL, dev, NULL, "%s", DEVNAME );
#endif
   }
   printk( KERN_INFO "======== module installed %d:[%d-%d] ===========\n",
           MAJOR( dev ), DEVICE_FIRST, MINOR( dev ) ); 
err:
   return ret;
}

static void __exit dev_exit( void ) {
   dev_t dev;
   int i;
   for( i = 0; i < DEVICE_COUNT; i++ ) {
      dev = MKDEV( major, DEVICE_FIRST + i );
      device_destroy( devclass, dev );
   }
   class_destroy( devclass );
   cdev_del( &hcdev );
   unregister_chrdev_region( MKDEV( major, DEVICE_FIRST ), DEVICE_COUNT );
   printk( KERN_INFO "=============== module removed ==================\n" );
}


static int __init dev_init( void );
module_init( dev_init );

static void __exit dev_exit( void );
module_exit( dev_exit );
