Task #6. Character device driver

Description∗
    Implement a character device driver for text messaging between users.

Subtask #1. Implement a character device driver with the following requirements:

    the default buffer size is 1 kB;
    set the buffer size by the module parameter;
    should be available for all users;
    works only ASCII strings.
    
Subtask #2. Add the interfaces (sysfs, procfs) into the driver:

    to select users;
    to clear the buffer;
    to get the buffer status (size, number of messages, amount of used space).
    
￼Implement bash script for the driver testing.

Provide a test scenario.

Extra∗
    Show messenger buffer status using 3.2inch RPi LCD V4 from GL Raspberry Kit.
    Use the interfaces (sysfs, procfs) to change the font, text and background color of the LCD.
