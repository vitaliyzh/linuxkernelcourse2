#!/bin/bash

show_dir ()
{
    echo "show_dir: "`ls -l`
}

while [ 1 ]; do
    read -p "Enter cmd: " CMD
    echo "Entered cmd: $CMD"
    if [ "$CMD" == "ls" ]; then
	ls .
    elif [ "$CMD" == "ls -f" ]; then
	ls -la
    elif [ "$CMD" == "ls -d" ]; then
	ls -lad
    elif [ "$CMD" == "cd" ]; then
	read -p "Enter dir name: " DIR
	if [ ! -d $CMD ]; then echo "Enter correct dirname"
	else
	    cd $DIR
	fi
    elif [ "$CMD" == "rename" ]; then
	read -p "Enter dir name src dst: " DIR1 DIR2
	rename $DIR1 $DIR2
    elif [ "$CMD" == "copy" ]; then
	read -p "Enter name src dst: " SRC DST
	if [ ! -f $SRC ]; then
	    echo "File $SRC not found"
	else
	    cp $SRC $DST
	fi
    elif [ "$CMD" == "remove" ]; then
	read -p "Enter dir name: " DIR1
	rm -dr $DIR1
    elif [ "$CMD" == "exit" ]; then
	break
    elif [ "$CMD" == "help" ]; then
	echo "List cmd: help, ls, ls -f, cd, rename, remove, exit"
    else
	echo "Unknow cmd: $CMD"
    fi
    show_dir
done