Task #9. Concurrency and Synchronization∗

Description∗
    Just spam me baby.

Guidance∗
    Use Git and the following names: task08 - for your project's home directory; partXX - for the directory of each subtask (XX - subtask number); src - for the source code directory.

Develop a kernel module to output specified messages at regular intervals using multiple threads.

    1⃣ Subtask #1. Output each message with a separate thread.
    2⃣ Set the parameters of each message using sysfs/procfs:
        message text,
        delay between each message (ms),
        total time for output of repeated messages (ms).
    3⃣ Subtask #2. Start the output by pressing a button connected to the GPIO pin (or any key of the standard keyboard).

Extra∗
    Use a fixed thread pool and job queue (lists).
    Turn on the LED connected to the GPIO pin for 500ms to signal the start of an output.
