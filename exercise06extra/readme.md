Exercise #6. Data Input/Output. Serial Interfaces

Extra∗
Подключить светодиодный модуль (RGB) из набора GL Raspberry Kit к Raspberry Pi Zero W используя выводы GPIO16, GPIO20, GPIO21.
Написать bash-скрипт для управления светодиодами:
все светодиоды выключены, программа ожидает первое нажатие на кнопку;
светодиоды включаются и выключаются поочередно с интервалом 0.5 сек.:
￼
R -> G -> B -> R ...
при каждом нажатии кнопки порядок светодиодов меняется на обратный:
￼
B -> G -> R -> B ...
Устранить дребезг контактов кнопки, если он присутствует.
