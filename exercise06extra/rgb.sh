#!/bin/bash 
#-x

GPIO_PATH=/sys/class/gpio

trap control_c SIGINT

control_c()
{
    echo "disable gpioS"
    set_port 16 0
    set_port 20 0
    set_port 21 0
    echo 16 > $GPIO_PATH/unexport
    echo 20 > $GPIO_PATH/unexport
    echo 21 > $GPIO_PATH/unexport
    echo 26 > $GPIO_PATH/unexport
    exit 0
}


init_port()
{
    PORT=$1
    DIRECT=$2
    echo "init port $PORT $DIRECT"
    if [ ! -d $GPIO_PATH/gpio$PORT ]; then
    {
       echo "enable gpio"
       `echo $PORT > $GPIO_PATH/export`
       sleep 0.1
       `echo $DIRECT > $GPIO_PATH/gpio$PORT/direction`
    }
    fi
}

set_port()
{
   ARG1=$1
   ARG2=$2
   #echo $ARG1 $ARG2
   echo $ARG2 > $GPIO_PATH/gpio$ARG1/value
}


init_port 26 in
init_port 16 out
init_port 20 out
init_port 21 out

COUNTER=0
OLD_VAL=0
VAL=0
POS_LAST=0
POS=0
RGB=(16 20 21)
ROUTE=0
STOPED=1
LIGHT_COUNTER=0
while [ 1 ]; do
  VAL=$(cat $GPIO_PATH/gpio26/value)
  if [ $OLD_VAL -eq 0 ] && [ $VAL -eq 1 ]; then
    STOPED=0
    OLD_VAL=$VAL
    if [ $ROUTE -eq 0 ]; then
      ROUTE=1
    else
      ROUTE=0
    fi
  else
    OLD_VAL=0
  fi
  
  let LIGHT_COUNTER=$LIGHT_COUNTER+1
  if [ $STOPED -eq 0 ] && [ $LIGHT_COUNTER -ge 5 ]; then
    set_port ${RGB[$POS]} 0
    if [ $ROUTE -eq 1 ]; then
      let POS=$POS+1

      if [ $POS -eq 3 ]; then
        POS=0
      fi
      echo + $POS
    else
      let POS=$POS-1

      if [ $POS -eq -1 ]; then
        POS=2
      fi
      echo - $POS
    fi
    set_port ${RGB[$POS]} 1
    LIGHT_COUNTER=0
    #sleep 0.4
  fi
  sleep 0.1
done




