Task #5. Virtual file system. Programming interface. Resources

￼Subtask #1. Implement the currency converter as a kernel module to...

    convert currency from/to Ukrainian hryvnia to/from euro.
    set the conversion factor based on the hryvnia.
    
￼Subtask #2. Add the interfaces into the module:

    procfs for currency conversion.
    sysfs to set conversion factors corresponding to the currencies used.
    
￼ Implement bash script for the module testing.

￼ Develop a C application to test module functionality.

￼ Provide a test scenario.

Extra∗
    Add the following functionality to the kernel module:
    conversion logging;
    dynamically add a new type of currency for conversion.
