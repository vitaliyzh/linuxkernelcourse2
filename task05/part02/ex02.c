#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/version.h>
#include <linux/cdev.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR( "Vitaliy Zhyrko vitaliyzh@gmail.com" );
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");

#define MODULE_TAG      "example_module "
#define PROC_DIRECTORY  "currency"
#define PROC_FILENAME   "uah"
#define PROC_FILENAME2   "eur"
#define BUFFER_SIZE     10
#define LEN_MSG 160

static long UAH = 0;
static long EUR = 0;
static int curse = 31;

u32 EUR_TO_UAH(int value)
{
    return value*curse;
}

u32 UAH_TO_EUR(int value)
{
    return (value*100/curse);
}

///sysfs

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,32)

#define IOFUNCS( name )                                                         \
static char buf_##name[ LEN_MSG + 1 ] = "0\n";                                  \
static ssize_t SHOW_##name( struct class *class, struct class_attribute *attr,  \
                            char *buf ) {                                       \
   strcpy( buf, buf_##name );                                                   \
   printk( "read %ld\n", (long)strlen( buf ) );                                 \
   return strlen( buf );                                                        \
}                                                                               \
static ssize_t STORE_##name( struct class *class, struct class_attribute *attr, \
                             const char *buf, size_t count ) {                  \
   int res;                                                                     \
   printk( "write %ld\n", (long)count );                                        \
   strncpy( buf_##name, buf, count );                                           \
   buf_##name[ count ] = '\0';                                                  \
   res = kstrtoint(buf_##name, 10, &curse);                                     \
   return count;                                                                \
}

#else

#define IOFUNCS( name )                                                         \
static char buf_##name[ LEN_MSG + 1 ] = "не инициализировано "#name"\n";        \
static ssize_t SHOW_##name( struct class *class, char *buf ) {                  \
   strcpy( buf, buf_##name );                                                   \
   printk( "read %ld\n", (long)strlen( buf ) );                                 \
   return strlen( buf );                                                        \
}                                                                               \
static ssize_t STORE_##name( struct class *class, const char *buf,              \
                             size_t count ) {                                   \
   printk( "write %ld\n", (long)count );                                        \
   strncpy( buf_##name, buf, count );                                           \
   buf_##name[ count ] = '\0';                                                  \
   return count;                                                                \
}
#endif

IOFUNCS( course );
#define OWN_CLASS_ATTR( name ) \
   struct class_attribute class_attr_##name = \
   __ATTR( name, ( S_IWUSR | S_IRUGO ), &SHOW_##name, &STORE_##name )
// ( S_IWUSR | S_IRUGO ),
//   __ATTR( name, 0666, &SHOW_##name, &STORE_##name )

static OWN_CLASS_ATTR( course );

static struct class *x_class=NULL;

static int create_sysclass(void)
{
    int res;
    x_class = class_create( THIS_MODULE, "course-class" );
    if( IS_ERR( x_class ) ) printk( "bad class create\n" );
    res = class_create_file( x_class, &class_attr_course );

    return res;
}

void x_cleanup(void) {
    if(x_class)
    {
        class_remove_file( x_class, &class_attr_course );
        class_destroy( x_class );
    }
}

///procfs
static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;
static struct proc_dir_entry *proc_file2;

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,6,0) && LINUX_VERSION_CODE > KERNEL_VERSION(3,10,0)
    static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
    static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);
    static ssize_t example_read2(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
    static ssize_t example_write2(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);
#else
    static int example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
    static int example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0)
static struct file_operations proc_fops = {
    .read  = example_read,
    .write = example_write,
};
#elseif LINUX_VERSION_CODE > KERNEL_VERSION(5,6,0)
static struct proc_ops proc_fops = {
    .proc_read  = example_read,
    .proc_write = example_write,
};
#else
static struct file_operations proc_fops = {
    .read  = example_read,
    .write = example_write,
};
static struct file_operations proc_fops2 = {
    .read  = example_read2,
    .write = example_write2,
};
#endif

static int create_buffer(void)
{
    proc_buffer = kmalloc(BUFFER_SIZE+1, GFP_KERNEL);
    if (NULL == proc_buffer)
        return -ENOMEM;
    proc_msg_length = 0;

    return 0;
}

static void cleanup_buffer(void)
{
    if (proc_buffer) {
        kfree(proc_buffer);
        proc_buffer = NULL;
    }
    proc_msg_length = 0;
}

static int create_proc_example(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    proc_file2 = proc_create(PROC_FILENAME2, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops2);
    if (NULL == proc_file2)
        return -EFAULT;

    return 0;
}

static void cleanup_proc_example(void)
{
    if (proc_file2)
    {
        remove_proc_entry(PROC_FILENAME2, proc_dir);
        proc_file2 = NULL;
    }
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,6,0) && LINUX_VERSION_CODE > KERNEL_VERSION(3,10,0)
    static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
#else
    static int example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
#endif
{
    printk(KERN_ERR MODULE_TAG "read");

    size_t left;

    if (length > (proc_msg_length - proc_msg_read_pos))
        length = (proc_msg_length - proc_msg_read_pos);

    char bufff[128];
    u32 value = EUR_TO_UAH(EUR);
    int count = snprintf(bufff, sizeof(bufff), "%d.%02d UAH%c", value, value % 1, '\n');
    bufff[count+1]=0;
    printk(KERN_ERR MODULE_TAG "currency rate: 1 EUR == %d UAH; result %s[len:%d] (%d->%d)", curse, bufff, count, EUR, value);

    left = raw_copy_to_user(buffer, &bufff, count);

    //left = raw_copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);

    proc_msg_read_pos += length - left;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to read %u from %u chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "read %u chars\n", length);

    return length - left;
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,6,0) && LINUX_VERSION_CODE > KERNEL_VERSION(3,10,0)
    static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
#else
    static int example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
#endif
{
    printk(KERN_ERR MODULE_TAG "write");
    size_t msg_length;
    size_t left;

    if (length > BUFFER_SIZE)
    {
        printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
        msg_length = BUFFER_SIZE;
    }
    else
        msg_length = length;

    left = raw_copy_from_user(proc_buffer, buffer, msg_length);

    proc_msg_length = msg_length - left;
    proc_msg_read_pos = 0;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %u from %u chars\n", left, msg_length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %u chars\n", msg_length);

    int err;
    proc_buffer[length]=0;
    err = kstrtol(proc_buffer, 10, &UAH);
    //if(err==0)
        printk(KERN_ERR MODULE_TAG "UAH: %d error:%d\n", UAH, err);

    return length;
}

static ssize_t example_read2(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
    printk(KERN_ERR MODULE_TAG "read2\n");

    size_t left;

    if (length > (proc_msg_length - proc_msg_read_pos))
        length = (proc_msg_length - proc_msg_read_pos);

    char bufff[128];
    u32 value = UAH_TO_EUR(UAH);
    int count = snprintf(bufff, sizeof(bufff), "%d.%02d EUR%c", value/100, value % 100, '\n');
    bufff[count+1]=0;
    printk(KERN_ERR MODULE_TAG "currency rate: 1 EUR == %d UAH; result %s[len:%d] (%d->%d)", curse, bufff, count, UAH, value);

    left = raw_copy_to_user(buffer, &bufff, count);
    //left = raw_copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);

    proc_msg_read_pos += length - left;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to read %u from %u chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "read %u chars\n", length);

    //flip_buffer_();

    return length - left;
}

static ssize_t example_write2(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    printk(KERN_ERR MODULE_TAG "write2\n");
    size_t msg_length;
    size_t left;

    if (length > BUFFER_SIZE)
    {
        printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
        msg_length = BUFFER_SIZE;
    }
    else
        msg_length = length;

    left = raw_copy_from_user(proc_buffer, buffer, msg_length);

    proc_msg_length = msg_length - left;
    proc_msg_read_pos = 0;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %u from %u chars\n", left, msg_length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %u chars\n", msg_length);

    int err;
    proc_buffer[length]=0;
    err = kstrtol(proc_buffer, 10, &EUR);
    //if(err==0)
        printk(KERN_ERR MODULE_TAG "EUR: %d error:%d\n", EUR, err);

    return length;
}

static int __init example_init(void)
{
    int err;

    err = create_buffer();
    if (err)
        goto error;

    err = create_proc_example();
    if (err)
        goto error;

    err = create_sysclass();
    if (err)
        goto error;

    printk(KERN_NOTICE MODULE_TAG "loaded\n");
    return 0;

error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    x_cleanup();
    cleanup_proc_example();
    cleanup_buffer();
    return err;
}

static void __exit example_exit(void)
{
    x_cleanup();
    cleanup_proc_example();
    cleanup_buffer();
    printk(KERN_NOTICE MODULE_TAG "exited\n");
}

module_init(example_init);
module_exit(example_exit);
