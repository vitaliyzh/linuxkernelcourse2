#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR( "Vitaliy Zhyrko vitaliyzh@gmail.com" );
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");

static int UAH = 0;
static int EUR = 0;
static int curse = 31;
module_param(UAH, int, S_IRUGO);
module_param(EUR, int, S_IRUGO);

u32 EUR_TO_UAH(int value)
{
    return value*curse;
}

u32 UAH_TO_EUR(int value)
{
    return value*100/curse;
}

static int __init ex01_init(void)
{
    u32 eur = UAH_TO_EUR(UAH);
    u32 uah = EUR_TO_UAH(EUR);

    printk(KERN_INFO "%u\n", (1%2) );
    printk(KERN_INFO "currency rate: 1 EUR == %u UAH\n", curse);
    printk(KERN_INFO "%u UAH -> %d.%02d EUR\n", UAH, eur/100, eur % 100);//(int)(eur/100), (int)(eur/100%100) );
    printk(KERN_INFO "%u EUR -> %d.%02d UAH\n", EUR, uah, uah % 1 );//(int)uah, (int)(uah%100) );
    return -1;
}

static void __exit ex01_exit(void)
{
	printk(KERN_INFO "Bye...\n");
}

module_init(ex01_init);
module_exit(ex01_exit);

