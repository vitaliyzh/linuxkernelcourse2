Exercise #13. Time Management

Description∗
    Implement examples as kernel modules.

Guidance∗
    Use Git and the following names: exercise13 - for your project's home directory; src - for the source code directory.

    Fix the errors and launch the modules.

    Analyze the results, describe them as comments on the source code.


