sudo dmesg -C
echo -e "\nstart tick" | sudo tee /dev/kmsg
sudo insmod ./tick.ko

echo -e "\nstart interv" | sudo tee /dev/kmsg
sudo insmod ./interv.ko
sleep 2
sudo rmmod interv

echo -e "\nstart hrtimer" | sudo tee /dev/kmsg
sudo insmod ./hrtimer.ko
sleep 2
sudo rmmod hrtimer

echo -e "\nstart mytimer" | sudo tee /dev/kmsg
sudo insmod ./mytimer.ko
sleep 2
sudo rmmod mytimer
dmesg