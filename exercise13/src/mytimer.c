/*******************************************************************************
* Copyright (c) 2015 Song Yang @ ittraining
* 
* All rights reserved.
* This program is free to use, but the ban on selling behavior.
* Modify the program must keep all the original text description.
*
* Email: onionys@ittraining.com.tw
* Blog : http://blog.ittraining.com.tw
*******************************************************************************/
#include <linux/module.h>
#include <linux/init.h>
#include <linux/timer.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/kernel.h>
#include <linux/version.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ITtraining.com.tw");
MODULE_DESCRIPTION("A timer example.");

struct timer_list my_timer;

/*
 * TIMER FUNCTION
 * */

#if (LINUX_VERSION_CODE < KERNEL_VERSION(4,15,0))
static void timer_function(unsigned long data){
    printk("Time up");
    // modify the timer for next time
    mod_timer(&my_timer, jiffies + HZ / 2);
#else
static void timer_function(struct timer_list *tl){
    struct timer_list * p = tl;
    printk("Time up");
    // modify the timer for next time
    // timer starts up every 0.5 sec(125 HZ)
    mod_timer(p, p->expires + HZ / 2);
#endif
}

/*
 * INIT MODULE
 * */
static int __init timer_init(void)
{
	printk("Hello My Timer\n");
#if (LINUX_VERSION_CODE < KERNEL_VERSION(4,15,0))
    //  -- initialize the timer
    init_timer(&my_timer);
    my_timer.expires = jiffies + HZ ;
    my_timer.function = timer_function;
    my_timer.data = NULL;

    // -- TIMER START
    add_timer(&my_timer);
#else
    timer_setup(&my_timer, timer_function, 0);
    mod_timer(&my_timer, jiffies + HZ);
#endif
	printk("END: init_module() \n");
	return 0;
}

/*
 * CLEANUP MODULE
 * */
static void __exit timer_exit(void)
{
    del_timer(&my_timer);
    //del_timer_sync(&my_timer);
	printk("Goodbye\n");
}

module_init( timer_init );
module_exit( timer_exit );
