#include "util.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define COUNT_PLAYERS 5
#define COUNT_ATTEMPTS 3

int main(void) {
  int ret, i, j;
  char name[32];
  struct list_node *head;
  struct list_node *cursor;
  struct gamer *element;

  head = NULL;
  for (i = 1; i < COUNT_PLAYERS + 1; ++i) {
    memset(name, 0, sizeof(name));
    snprintf(name, sizeof(name), "gamer%02d", i);
    element = create_gamer(i, name);
    head = list_add(&element->list, head);
    printf("add %s\n", name);
  }

  cursor = head;
  list_for_each(cursor) {
    element = container_of(cursor, struct gamer, list);
    if (element) {
      for (j = 0; j < COUNT_ATTEMPTS; ++j) {
        int points = dice_roll();
        // printf("= %d+%d=%d =\n", element->number_of_points, points,
        // element->number_of_points+points);
        element->number_of_points += points;
        element->last_time = time(NULL);
      }
    }
  }

  cursor = head;
  list_for_each(cursor) {
    if (element) {
      element = container_of(cursor, struct gamer, list);
      printf("[%02d] %s - points: %d\n", element->id, element->name,
             element->number_of_points);
    }
  }
  // ret = dice_roll();
  // printf("= %d =\n", ret);

  return 0;
}
