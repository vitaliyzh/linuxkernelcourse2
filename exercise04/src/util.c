#include "util.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/times.h>
#include <time.h>

struct list_node *list_add(struct list_node *element, struct list_node *head) {
  element->next = head;
  return element;
}

struct gamer *create_gamer(int id, const char *name) {
  struct gamer *res = malloc(sizeof(struct gamer));
  res->id = id;
  strncpy(res->name, name, sizeof((res->name)));
  res->list.next = NULL;
  res->last_time = 0;
  res->number_of_points = 0;
  return res;
}

struct gamer *find_gamer(int id, struct list_node *head) {
  struct gamer *res = NULL;

  if (head) {
    struct gamer *element;
    struct list_node *cursor;
    printf("find elements\n");
    for (cursor = head; cursor != NULL; cursor->next) {
      element = container_of(cursor, struct gamer, list);
      printf("element %d (%d)\n", element->id, id);
      if (element->id == id) {
        res = element;
        break;
      }
    }
  }

  return res;
}

int dice_roll() {
  int ret = 0;
  int min = 1;
  int max = 6;
  static bool first = true;
  if (first) {
    srand(times(NULL));
    first = false;
  }

  for (;;) {
    ret = min + rand() % ((max + 1) - min);
    // printf("%d\n", ret);
    if (ret > 0 && ret < 7)
      break;
  }
  return ret;
}
