#include <time.h>

struct list_node {
  struct list_node *next;
};

struct gamer {
  struct list_node list;
  char name[32];
  int id;
  time_t last_time;
  unsigned int number_of_points;
};

// gamer::gamer()
//{
//    name = "";
//    id = 0;
//    last_time = 0;
//    number_of_points = 0;
//}

#define container_of(ptr, type, member)                                        \
  ((type *)((char *)(ptr)-offsetof(type, member)))

#define list_for_each(pos) for (; pos != NULL; pos = pos->next)

struct list_node *list_add(struct list_node *element, struct list_node *head);

struct gamer *create_gamer(int id, const char *name);

struct gamer *find_gamer(int id, struct list_node *head);

int dice_roll();
